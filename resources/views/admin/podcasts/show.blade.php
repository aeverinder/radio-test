@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Questionnaire review</div>

                    <div class="panel-body">
                        {{--Pulling the title data from the survey title field in the survey table--}}
                        <h1>{{ $survey->title }}</h1>
                        {{--Pulling the content data from the survey title field in the survey table--}}
                        <p> Content: {{ $survey->content }}</p>

                        {{--Opening up the form and pulling the store function from the results controller--}}
                        {!! Form::open(array('action' => 'ResultsController@store', 'id' => 'resultarticle')) !!}

                        <p></p>

                        {{--Pulling the question 1 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_1 }}</h3>

                        {{--Pulling the question 2 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_2 }}</h3>

                        {{--Pulling the question 3 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_3 }}</h3>
                        possible answers:

                        {{--Pulling the question 4 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_4 }}</h3>
                        possible answers:

                        {{--Pulling the question 5 data from the survey title field in the survey table--}}
                        <h3> {{ $survey->question_5 }}</h3>
                        possible answers:

                        {{--Pulling the creator from the survey table in the database--}}
                        <p>creator: {{$survey->author->name}}</p>
                        {{--The form submit button for the show view--}}
                        <div class="row large-4 columns">
                            {!! Form::submit('End questionnaire', ['class' => 'button']) !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection