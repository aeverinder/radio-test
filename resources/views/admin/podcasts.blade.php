@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Podcasts</div>

                    <div class="panel-body">
                        Podcasts ready to be enjoyed
                        <section>
                            @if (isset ($podcasts))
                                <ul>
                                    @foreach ($podcasts as $podcast)
                                        <li><a href="/admin/podacts/{{ $podcast->id }}" name="{{ $podcast->title }}">{{ $podcast->title }}</a></li>
                                      <!--  <td><a href="/admin/podcasts/result/{{  $podcast->id }}" name="{{ $podcast->title }}">view Results</a></td> -->
                                    @endforeach

                                </ul>
                            @else
                                <p> no podcasts added yet </p>
                            @endif
                        </section>

                        {{ Form::open(array('action' => 'PodcastController@create', 'method' => 'get')) }}
                        <div class="row">
                            {!! Form::submit('Add Podcast', ['class' => 'button']) !!}
                        </div>
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection