@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Podcast</div>

                    <div class="panel-body">



                        {{--Opening up the form and pulling the store function from the podcast controller--}}
                        {!! Form::open(array('action' => 'PodcastController@store', 'id' => 'createpodcast')) !!}

                        <div class="row large-12 columns">
                            {!! Form::label('title', 'Title:') !!}
                            {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('url', 'url:') !!}
                            {!! Form::text('url', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('description', 'Description 1:') !!}
                            {!! Form::text('description', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        <div class="row large-12 columns">
                            {!! Form::label('episodenumber', 'Episode number:') !!}
                            {!! Form::text('episodenumber', null, ['class' => 'large-8 columns']) !!}
                        </div>

                        {{--This submits the podcast --}}
                        <div class="row large-4 columns">
                            {!! Form::submit('Add Podcast', ['class' => 'button']) !!}
                        </div>

                        {{--Closing the form--}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
