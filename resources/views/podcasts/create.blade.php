@extends('layouts.master')

@section('title', 'My Home Page')

@section('content')
<div class="container">
<style>
table, th, td {
  border: 1px solid white;
  padding: 10px;
}
table {
  border-spacing: 15px;
  text-align: center;
}
th {
    text-align: center;
}
</style>
    <div class="container">
      <h2>Create A podcast</h2><br  />
      <a href="{{ url('/podcasts') }}">view a podcast</a>
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif
      <form method="post" action="{{url('podcasts')}}">
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="url">url:</label>
              <textarea placeholder="enter url"
                        style="resize: vertical"
                        id="url"
                        name="url"
                        rows="5" spellcheck="false"
                        class="form-control">
                      </textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="descriptio">description:</label>
              <input type="text" class="form-control" name="description">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="episodenumber">episode number:</label>
              <input type="date" class="form-control" name="episodenumber">
            </div>
          </div>
      </form>
    </div>
