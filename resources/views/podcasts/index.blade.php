<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <a href="{{ url('/podcasts/create') }}">create a podcast</a>
    <div class="container">
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif
    <!-- <table class="table table-striped"> -->


        <!-- <th colspan="2">Action</th> -->
        <div class="container">
      @foreach($podcasts as $podcast)
      <div class="panel-heading">ID:  {{$podcast['id']}}</div>
        <div class="panel-body">
        <p>name:  {{$podcast['name']}}</p>
        <p>test:  {{$podcast['url']}}</p>
        <p>project title: {{$podcast['description']}}</p>
          <button class=find"><a href="{{asset($podcast->podcast_link)}}">View podcast</a></button>
        </div>
      </div>
          <form action="{{action('PodcastController@destroy', $podcast['id'])}}" method="post">
            {{csrf_field()}}
            <td><a href="{{action('PodcastController@edit', $podcast['id'])}}" class="btn btn-warning">Edit</a></td>
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
