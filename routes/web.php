<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/podcast', 'PodcastController@index')->name('podcast');
Route::get('/podcasts/create', 'PodcastController@create')->name('podcast');

Route::post('podcast-upload',['as'=>'podcast.upload.post','uses'=>'PodcastController@image_upload']);

Route::resource('/podcast', 'PodcastController');
//Route::post('/podcasts', 'PodcastController@image_upload');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'web'], function () {
    Route::auth();
});
