<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('update an podcast');

// Add db test data

// add a test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);


// add a test Podcast to check that content can be seen in list at start

$I->haveRecord('podcasts', [
    'id' => '9000',
    'title' => 'Podcast 1',
    'description' => 'podcast 1 descriptiom',
    'user_id' => 'testuser1',
]);


// tests /////////////////////////////////////////////

// create an Podcast linked to one category
// When
$I->amOnPage('/admin/podcasts');
$I->see('Podcast', 'h1');
$I->see('podcast 1');


// Then

// Check  the link is present - this is because there could potentially be many update links/buttons.
// each link can be identified by the users id as name.
$I->seeElement('a', ['name' => '9000']);
// And
$I->click('a', ['name' => '9000']);

// Then
$I->amOnPage('/admin/podcasts/9000/edit');
// And
$I->see('Edit Podcast - Podcast1', 'h1');

// Then
$I->fillField('title', 'Updatedtitle');
// And
$I->click('Update Podcast');

// Then
$I->seeCurrentUrlEquals('admin/podcasts');
$I->seeRecord('podcasts', ['title' => 'Updatedtitile']);
$I->see('Podcasts', 'h1');
$I->see('Podcasts', 'h1');
$I->see('Updatedtitile');