<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new podcast');

// log in as your admin user
// This should be id of 1 if you created your manual login for a known user first.
Auth::loginUsingId(1);
// Add db test data
// Add db test data

// add a test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);



// add a test podcast to check that content can be seen in list at start

$I->haveRecord('podcasts', [
    'id' => '9000',
    'title' => 'Podcast 1',
    'descriptiom' => 'podcast 1 description',
    'author_id' => '9999',
]);


// tests /////////////////////////////////////////////

// create an podcast linked to one category
// When
$I->amOnPage('/admin/podcasts');
$I->see('Podcasts', 'h1');
$I->see('Podcast 1');
$I->dontSee('Podcast 2');
// And
$I->click('Add Podcast');

// Then
$I->amOnPage('/admin/podcasts/create');
// And
$I->see('Add Podcast', 'h1');
$I->submitForm('#createpodcast', [
    'title' => 'Podcast 2',
    'content' => 'Podcast 2 content',
    'category' => '9900',
]);

// how to handle the link table checking.

// check that the podcast has been written to the db then grab the new id ready to use as input to the link table.
$article = $I->grabRecord('podcasts', ['title' => 'Article 2']);

// Then
$I->seeCurrentUrlEquals('/admin/podcasts');
$I->see('Podcasts', 'h1');
$I->see('Podcast 1');
$I->see('Podcast 2');

$I->click('Podcast 2'); // the title is a link to the detail page


// Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
$I->seeCurrentUrlMatches('~/admin/podcasts/(\d+)~');
$I->see('Podcast 2', 'h1');
$I->see('Podcast 2 content');
$I->see('creator: testuser1');
$I->see('categories:');
$I->see('category 1');



// create an podcast linked to two categories
// When
$I->amOnPage('/admin/podcasts');
$I->see('Podcasts', 'h1');
$I->see('Podcast 1');
$I->dontSee('Podcast 3');
// And
$I->click('Add Podcast');

// Then
$I->amOnPage('/admin/podcasts/create');
// And
$I->see('Add Podcast', 'h1');
$I->submitForm('.createpodcast', [
    'title' => 'Podcast 3',
    'content' => 'Podcast 3 content',
    'author_id' => '9999',
]);

// how to handle the link table checking.




// Then
$I->seeCurrentUrlEquals('/admin/podcasts');
$I->see('Podcast', 'h1');
$I->see('Podcast 1');
$I->see('Podcast 2');
$I->see('Podcast 3');

$I->click('Article 3'); // the title is a link to the detail page


// Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
$I->seeCurrentUrlMatches('~/admin/podcasts/(\d+)~');
$I->see('Podcast 2', 'h1');
$I->see('podcast 2 content');
$I->see('creator: testuser1');
