<?php

namespace App\Http\Controllers;
use App\Podcast;

use Illuminate\Http\Request;

class PodcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $podcasts = Podcast::all()->toArray();
        return view('podcasts.index', compact('podcasts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('podcasts.create_2');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function image_upload(Request $request){
        //for security i am naming the file by time of upload
        $PodcastName = time() . '.' . request()->podcast->getClientOriginalExtension();
        request()->podcast->move(public_path('storage/podcast'), $PodcastName);




        $podcast = 'storage/podcast' . $PodcastName;
        return view('/home');
    }
    public function store(Request $request)
    {
        $podcast = $this->validate(request(), [
            'name' => 'required',
            'url' => 'required',
            'description' => 'required',
            'episode number' => 'required',
        ]);
        Podcast::create($podcast);
        return back()->with('success', 'Podcast has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $podcast = Podcast::find($id);
        return view('Podcast.edit',compact('podcast','id'));
    }


    public function update(Request $request, $id)
    {
        $podcast = Podcast::find($id);
        $this->validate(request(), [
            'name' => 'required',
            'url' => 'required',
            'description' => 'required',
            'episode number' => 'required',

        ]);

        $podcast->name = $request->get('name');
        $podcast->url = $request->get('url');
        $podcast->description = $request->get('description');
        $podcast->episodenumber = $request->get('episode number');
        $podcast->save();
        return redirect('podcasats')->with('success','Podcast has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $podcast = Podcast::find($id);
        $podcast->delete();
        return redirect('podcast')->with('success','Podcast has been  deleted');
    }
}
